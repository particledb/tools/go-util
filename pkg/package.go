package pkg

import (
	"os"
	"strings"
)

type Package struct {
	Dir          string
	ImportPath   string
	Name         string
	Target       string
	Stale        bool
	StaleReason  string
	Root         string
	GoFiles      []string
	Imports      []string
	Deps         []string
	TestGoFiles  []string
	TestImports  []string
	XTestGoFiles []string
	XTestImports []string
}

func (p *Package) Skip(except []string) bool {
	for _, except := range except {
		if except == p.Name || except == p.ImportPath {
			return true
		}
	}

	return false
}

func (p *Package) IsVendored() bool {
	for _, bit := range strings.Split(p.Dir, string(os.PathSeparator)) {
		if bit == "vendor" {
			return true
		}
	}
	return false
}
