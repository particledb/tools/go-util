package pkg_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/particledb/tools/go-util/pkg"
	"testing"
)

func TestListCurrentPackageName(t *testing.T) {
	assert.Equal(t, "gitlab.com/particledb/tools/go-util/pkg", pkg.CurrentPackagePath())
}

func TestListPackages(t *testing.T) {
	pkg := pkg.ListPackages().Single()
	assert.Equal(t, "pkg", pkg.Name)
	assert.Equal(t, "gitlab.com/particledb/tools/go-util/pkg", pkg.ImportPath)
}

func TestSkipPackage(t *testing.T) {
	pkg := pkg.ListPackages().Single()
	assert.False(t, pkg.Skip([]string{}))
	assert.True(t, pkg.Skip([]string{"pkg"}))
	assert.True(t, pkg.Skip([]string{"gitlab.com/particledb/tools/go-util/pkg"}))
}
