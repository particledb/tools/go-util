package pkg

type Packages chan *Package

func (p Packages) Single() *Package {
	var i = 0
	var pkg *Package

	for pkg = range p {
		if i == 0 {
			i++
		} else {
			panic("expected exactly one package")
		}
	}

	if i == 0 {
		panic("expected exactly one package")
	}

	return pkg
}

func (p Packages) SingleOrDefault() *Package {
	var i = 0
	var pkg *Package

	for pkg = range p {
		if i == 0 {
			i++
		} else {
			panic("expected at least one package")
		}
	}

	return pkg
}
