package pkg

import (
	"bytes"
	"encoding/json"
	"os"
	"os/exec"
	"strings"
)

func CurrentPackagePath() string {
	out, err := exec.Command("go", "list").Output()
	if err != nil {
		panic(err)
	}

	return strings.TrimSpace(string(out))
}

func ListPackages(args ...string) (pkgs Packages) {
	pkgs = make(Packages)

	args = append([]string{"list", "-json"}, args...)
	cmd := exec.Command("go", args...)
	cmd.Stderr = os.Stderr

	out, err := cmd.Output()
	if _, ok := err.(*exec.ExitError); err != nil && !ok {
		panic(err)
	}

	buf := bytes.NewBuffer(out)
	dec := json.NewDecoder(buf)

	go func() {

		for dec.More() {
			var pkg *Package
			if err := dec.Decode(&pkg); err != nil {
				panic(err)
			} else {
				pkgs <- pkg
			}
		}
		close(pkgs)
	}()

	return
}
