package sys

import (
	"os"
	"path/filepath"
	"strings"
)

func Getwd() string {
	wd, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	for x := 0; x < 1000; x++ {
		// get indexes of all of the slashes
		indexes := []int{}
		for i, c := range wd {
			if i == 0 || i == len(wd)-1 {
				continue
			}
			if c == '/' {
				indexes = append(indexes, i)
			}
		}

		// check directories in reverse for sym-link status
		symlink := false
		for i := len(indexes) - 1; i >= 0; i-- {
			j := indexes[i]
			path, err := os.Readlink(wd[:j])
			if err != nil {
				continue
			}
			symlink = true
			wd = path + wd[j:]
		}

		if !symlink {
			break
		}
	}

	return wd
}

func GetCurrentImportPath() string {
	wd := Getwd()

	gopaths := os.Getenv("GOPATH")
	if gopaths == "" {
		panic("GOPATH is not set")
	}

	for _, gopath := range filepath.SplitList(gopaths) {
		if strings.HasPrefix(wd, gopath+"/src/") {
			return wd[len(gopath)+5:]
		}
	}

	panic("working directory is not within GOPATH")
}

func ResolveRelativePackages(pkgs []string) {
	cpkg := GetCurrentImportPath()

	for i, pkg := range pkgs {
		if !strings.HasPrefix(pkg, "./") {
			continue
		}

		pkgs[i] = cpkg + pkg[1:]
	}
}
