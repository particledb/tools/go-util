package sys_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/particledb/tools/go-util/sys"
	"os"
	"testing"
)

func TestGetCurrentImportPath(t *testing.T) {
	assert.Equal(t, "gitlab.com/particledb/tools/go-util/sys", sys.GetCurrentImportPath())
}

func TestResolveRelativePackages(t *testing.T) {
	if wd, err := os.Getwd(); err != nil {
		t.Fatal(err)
	} else if err := os.Chdir("../"); err != nil {
		t.Fatal(err)
	} else {
		defer func() {
			if err := os.Chdir(wd); err != nil {
				panic(err)
			}
		}()
	}

	pkgs := []string{"./cmd", "./pkg", "./sys"}
	sys.ResolveRelativePackages(pkgs)
	assert.Equal(t, []string{
		"gitlab.com/particledb/tools/go-util/cmd",
		"gitlab.com/particledb/tools/go-util/pkg",
		"gitlab.com/particledb/tools/go-util/sys",
	}, pkgs)

}
