package cmd_test

import (
	"gitlab.com/particledb/tools/go-util/cmd"
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	os.Chdir("../")
	os.Exit(m.Run())
}

func TestCiDeps(t *testing.T) {
	cmd.CiDeps([]string{}, true)
}

func TestTestAll(t *testing.T) {
	cmd.TestAll([]string{"./cmd"}, true)
}

func TestBuildAll(t *testing.T) {
	cmd.BuildAll([]string{"./cmd"}, true)
}
