package cmd

import (
	"fmt"
	"gitlab.com/particledb/tools/go-util/pkg"
	"gitlab.com/particledb/tools/go-util/sys"
	"os"
	"os/exec"
	"path"
	"strings"
)

func CiDeps(except []string, pretend bool) {
	cpkg := sys.GetCurrentImportPath()
	deps := make(map[string]bool)

	isRemote := func(dep string) bool {
		root := strings.Split(dep, "/")[0]
		return strings.Contains(root, ".") && !strings.HasPrefix(dep, cpkg)
	}

	collect := func(pkg *pkg.Package) {
		for _, dep := range pkg.Imports {
			deps[dep] = isRemote(dep)
		}
		for _, dep := range pkg.TestImports {
			deps[dep] = isRemote(dep)
		}
		for _, dep := range pkg.XTestImports {
			deps[dep] = isRemote(dep)
		}
	}

	for pkg := range pkg.ListPackages("./...") {
		if pkg.IsVendored() {
			continue
		}

		collect(pkg)

		if pkg.ImportPath == cpkg {
			continue
		}

		deps[pkg.ImportPath] = isRemote(pkg.ImportPath)
	}

	repos := make(map[string]bool)
	findRepo := func(dep string) {
		for {
			p := path.Join(os.Getenv("GOPATH"), "src", dep, ".git")
			if _, err := os.Stat(p); err == nil || !os.IsNotExist(err) {
				repos[dep] = true
				return
			}

			dir, _ := path.Split(dep)
			if dir == "" {
				return
			}
			dep = dir[:len(dir)-1]
		}
	}

	for dep, remote := range deps {
		if !remote {
			continue
		}

		findRepo(dep)
	}

	for repo := range repos {
		fmt.Println("*** updating", repo)

		if pretend {
			continue
		}

		cmd := exec.Command("go", "get", "-d", "-u", "-f", "-t", repo)
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr

		if err := cmd.Run(); err != nil {
			fmt.Println("failed: ", err)
		}
	}
}

func TestAll(except []string, pretend bool) {
	sys.ResolveRelativePackages(except)

	for pkg := range pkg.ListPackages("./...") {
		if pkg.IsVendored() {
			continue
		}

		if len(pkg.TestGoFiles) == 0 && len(pkg.XTestGoFiles) == 0 {
			continue
		}

		if pkg.Skip(except) {
			continue
		}

		fmt.Println("*** testing", pkg.ImportPath)

		if pretend {
			continue
		}

		cmd := exec.Command("go", "test", pkg.ImportPath)
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr

		if err := cmd.Run(); err != nil {
			panic(err)
		}
	}
}

func BuildAll(except []string, pretend bool) {
	sys.ResolveRelativePackages(except)

	for pkg := range pkg.ListPackages("./...") {
		if pkg.IsVendored() {
			continue
		}

		if len(pkg.GoFiles) == 0 {
			continue
		}

		if pkg.Skip(except) {
			continue
		}

		fmt.Println("*** building", pkg.ImportPath)

		if pretend {
			continue
		}

		cmd := exec.Command("go", "build", pkg.ImportPath)
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr

		if err := cmd.Run(); err != nil {
			panic(err)
		}
	}
}
