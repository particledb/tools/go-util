package main

import (
	"fmt"
	"github.com/alecthomas/kingpin"
	"gitlab.com/particledb/tools/go-util/cmd"
)

const VER = "0.8.1"

var (
	exceptFlag  = kingpin.Flag("except", "omit packages").Short('x').Strings()
	pretendFlag = kingpin.Flag("pretend", "pretend").Short('n').Bool()
	versionCmd  = kingpin.Command("version", "print version")
	ciDepsCmd   = kingpin.Command("ci-deps", "CI: update dependencies")
	testAllCmd  = kingpin.Command("test-all", "test all packages")
	buildAllCmd = kingpin.Command("build-all", "build all packages")
)

func main() {
	switch kingpin.Parse() {
	case "version":
		fmt.Println(VER)
	case "ci-deps":
		cmd.CiDeps(*exceptFlag, *pretendFlag)
	case "test-all":
		cmd.TestAll(*exceptFlag, *pretendFlag)
	case "build-all":
		cmd.BuildAll(*exceptFlag, *pretendFlag)
	}
}
